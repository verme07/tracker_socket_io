

const express = require('express');
const path = require('path');


const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const geolocation = require('geolocation-utils');

app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'public'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use('/', function (req, res) {
    res.render('index.html');
});

console.log("Running App");
var mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "champier",
    password: "E2Y>FK8f:FG?4GGY",
    database: "prm"
});


io.on('connection', socket => {

    console.log("Socket Conectado! " + socket.id);

    socket.on('retrieveAllBuses', data => {
        retrieveAllBuses(function (result) {
            var messages = JSON.stringify(result);
            var newList = JSON.parse(messages);
            var nearBuses = []

            for(var i in newList){
                let location1 = {lat: newList[i].latitude, lon: newList[i].longitude}
                let location2 = {lat: parseFloat(data.latitude), lon: parseFloat(data.longitude) }
                let distance = geolocation.headingDistanceTo(location1, location2);

                if (distance.distance < 2000){
                    console.log(distance.distance)
                    nearBuses.push(newList[i])
                }
            };
            let buses = JSON.stringify(nearBuses);
            console.log("Emmiting "+data.channel_id);
            socket.emit('channel_' + data.channel_id, buses);
        });
    });
    socket.on('retrieveAllCars', data => {

        retrieveAllCars( function (result) {
            var busData = JSON.stringify(result);
            console.log(""+busData);
            socket.emit('channel_' + data.channel_id, busData);
        });
    });





});


function retrieveAllCars(callback) {

    var sql = "SELECT * FROM vehicles"

    con.query(sql, function (err, result, fields) {
        if (err) {
            throw err;
        }

        if (callback){
            callback(result)
        }

    });
}

server.listen(3020);